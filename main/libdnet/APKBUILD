# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=libdnet
pkgver=1.16.2
pkgrel=0
pkgdesc="simplified, portable interface to several low-level networking routines"
url="https://github.com/ofalk/libdnet"
arch="all"
license="BSD-3-Clause"
makedepends="autoconf automake libtool check-dev linux-headers"
options="!check" # fails to find test-driver
subpackages="$pkgname-dev $pkgname-doc"
source="https://github.com/ofalk/libdnet/archive/libdnet-$pkgver.tar.gz"
builddir="$srcdir"/$pkgname-$pkgname-$pkgver

prepare() {
	default_prepare

	# ./compile missing in tarball
	autoreconf -fvi
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--mandir=/usr/share/man \
		--without-python
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="
4b1902553a57eeb56952968e15be032de92d8106dc6e0ebf8e10470605c9c2ed69cb015f4057a5c119d01509c6795fc0dcda85a311d14124dddefdeb6223f848  libdnet-1.16.2.tar.gz
"
