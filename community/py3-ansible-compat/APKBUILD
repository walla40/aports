# Contributor: Sean McAvoy <seanmcavoy@gmail.com>
# Maintainer: Sean McAvoy <seanmcavoy@gmail.com>
pkgname=py3-ansible-compat
pkgver=2.2.7
pkgrel=0
pkgdesc="functions that help interacting with various versions of Ansible"
url="https://github.com/ansible/ansible-compat"
arch="all"
license="MIT"
depends="python3 py3-subprocess-tee py3-yaml"
makedepends="
	ansible-core
	py3-gpep517
	py3-installer
	py3-setuptools
	py3-setuptools_scm
	py3-wheel
	"
checkdepends="py3-pytest py3-flaky py3-pytest-mock"
#subpackages="$pkgname-doc"
source="ansible-compat-$pkgver.tar.gz::https://files.pythonhosted.org/packages/source/a/ansible-compat/ansible-compat-$pkgver.tar.gz"
builddir="$srcdir/ansible-compat-$pkgver"
# the tests don't clean up after themselves, and fail if something is left in
# /tmp, and every release they add even more broken tests like this
options="!check"

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 1
}

check() {
	# FIXME: bad tests leave around state and fail on builders
	# on subsequent runs
	PYTHONPATH="$builddir/build/lib:$PYTHONPATH" pytest -v \
		-k "not test_runtime_install_role and not test_install_galaxy_role"
}

package() {
	python3 -m installer --destdir="$pkgdir" --compile-bytecode 0 \
		dist/*.whl
}

sha512sums="
9a3863be3d755eb5a1a61ba0c526786ea3df14bc6fcabd9196559eb625ab6dfc8de7a17fdb2cdd8ad71d7d61b5e2f0543b8b5908f39d5e7d9ea45e387aa625b1  ansible-compat-2.2.7.tar.gz
"
