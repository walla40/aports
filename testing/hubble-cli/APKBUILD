# Maintainer: Hoang Nguyen <folliekazetani@protonmail.com>
pkgname=hubble-cli
pkgver=0.11.0
pkgrel=0
pkgdesc="CLI for the distributed networking and security observability platform"
url="https://github.com/cilium/hubble"
arch="all"
license="Apache-2.0"
makedepends="go"
subpackages="
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/cilium/hubble/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/hubble-$pkgver"

export CGO_ENABLED=0
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build -v \
		-ldflags "-X github.com/cilium/hubble/pkg.Version=$pkgver" \
		-o hubble

	for shell in bash fish zsh; do
		./hubble completion $shell > hubble.$shell
	done
}

check() {
	go test ./...
}

package() {
	install -Dm755 hubble -t "$pkgdir"/usr/bin/

	install -Dm644 hubble.bash \
		"$pkgdir"/usr/share/bash-completion/completions/hubble
	install -Dm644 hubble.fish \
		"$pkgdir"/usr/share/fish/completions/hubble.fish
	install -Dm644 hubble.zsh \
		"$pkgdir"/usr/share/zsh/site-functions/_hubble
}

sha512sums="
fe0461ded7ed1795995f12feef4ea8dc24e7ee2d4b0ff8d09d8aa702304f27325b875886f805741e23674f20e9380bc15d46c14ddb4b5395c4d939504f6ed88a  hubble-cli-0.11.0.tar.gz
"
